---
lang: en-US
title: 3D Character Animation in Unity
description: Tutorial on importing a character and animating in Unity
---

# {{ $frontmatter.title }}

This quick tutorial will go over how to animate your 3D character in Unity. After downloading your model from our web browser, follow our video instructions and you'll Quickly be able to animate your character!

See the [original guide](/guides/configure-unity-import.html) for additional information and answers to questions.

<div class="video-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/0TmQWjoON3E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
