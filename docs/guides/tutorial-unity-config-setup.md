---
lang: en-US
title: 3D Character Unity Importing
description: Tutorial on camera control
---

# {{ $frontmatter.title }}

This quick tutorial will go over how to import your recently made 3D character into Unity. After downloading your model from our web browser, follow our video instructions and you'll Quickly be able to import your character!

<div class="video-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/rpxEy6mQQgY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
