---
lang: en-US
title: Unity character import
description: Configure Unity for 1-click importing of quickytools characters
sidebar: auto
---

Follow these steps to enable 1-click integration of quickytools Characters in Unity.

### Start

1. Install Unity.
1. Open an existing Unity project or create a new Unity project.
1. Fix all errors in the Console tab.
1. Exit play mode if currently playing. The shortcut for entering and exiting play mode is Cmd+P or Ctrl+P.  
   <img :src="$withBase('/images/unity-auto-extract/unity-play-off.png')" alt="Not in play mode">
1. Change Project tab display to One Column Layout (to match this guide). This will show files and folders in the project tab rather than only folders.  
   <img :src="$withBase('/images/unity-auto-extract/unity-project-tab-one-column.png')" alt="Change project display to one column">

### Install quickytools Character Unity plugin
There are two methods to install the plugin in Unity. <button onclick="document.querySelector('#installPackage').style.display='block';document.querySelector('#installGit').style.display='none'">Import the plugin</button> with a `.unitypackage` file or <button onclick="document.querySelector('#installGit').style.display='block';document.querySelector('#installPackage').style.display='none'">use git</button> (requires git be installed on your machine).

<div id='installPackage' style='display: block'>
  <h3>Import plugin</h3>
  <ol>
    <li>
      <p>Uninstall/delete any previous version of the quickytools character setup plugin or package in Unity.</p>
    </li>
    <li>
      <p>In Unity open Package Manager.</p>
      <img :src="$withBase('/images/unity-auto-extract/unity-window-package-manager.png')" alt="Package Manager" />
    </li>
    <li>
      <p>Install the Input System package under <code>Packages: Unity Registry</code>. Use the search bar to filter packages by name. The version and information may be different from what is shown in the the screenshot.</p>
      <img :src="$withBase('/images/unity-auto-extract/unity-input-system-package.png')" alt="Input System package" />
    </li>
    <li>
      <p>Download the latest <a href="https://gitlab.com/quickytools-open-source/character-setup-unitypackage" target="_blank">quickytools-character-setup-X.Y.Z.unitypackage</a>.
      </p>
    </li>
    <li>
      <p>Import the package into Unity with <code>Assets &gt; Import Package &gt; Custom Package...</code> selecting the downloaded <code>.unitypackage</code> when prompted.</p>
      <img :src="$withBase('/images/unity-auto-extract/unity-import-custom-package.png')" alt="Import custom package" />
    </li>
    <li>
      <p>The installed plugin files should be structured as follows. Avoid making changes to these files unless you need custom behavior.
      </p>
      <img :src="$withBase('/images/unity-auto-extract/unity-quickytools-character-package-dir.png')" alt="Plugin project files">
    </li>
  </ol>
</div>

<div id='installGit' style='display: none'>
  <h3>Install git package</h3>
  <ol>
    <li>
      <p>In Unity open Package Manager.</p>
      <img :src="$withBase('/images/unity-auto-extract/unity-window-package-manager.png')" alt="Package Manager" />
    </li>
    <li>
      <p>
        Press the <code>+</code> dropdown and <code>Add package from git URL...</code>=<code>https://gitlab.com/quickytools-open-source/extract-character-unity.git</code>.
      </p>
      <img :src="$withBase('/images/unity-auto-extract/unity-add-git-package.png')" alt="Add git package" />
    </li>
    <li>
      <p>The plugin shows once installed successfully. Errors (see below) may need addressing before this shows up.</p>
      <img :src="$withBase('/images/unity-auto-extract/unity-character-extract-package-installed.png')" alt="Package installed" />
    </li>
  </ol>
</div>

### Fix/resolve errors and changes

If errors are listed in the Console tab fix them or Unity won't work correctly moving forward. Warnings can be ignored as they won't get in the way.
- If you created a new Unity project packages can be removed or downgraded without issue. Cinemachine and TextMeshPro are not needed by the quickytools Character plugin. Downgrade by expanding a package, pressing `See other versions`, selecting a version, and pressing `Update to X.Y.Z`.  
  <img :src="$withBase('/images/unity-auto-extract/unity-packages-other-versions.png')" height="240" alt="Other package versions"> 
  <img :src="$withBase('/images/unity-auto-extract/unity-package-downgrade.png')" height="240" alt="Downgrade package version">
- Errors about "collab.proxy" or similar are likely due to the Version Control package. Remove this package if you don't use it.  
  <img :src="$withBase('/images/unity-auto-extract/unity-version-control-package.png')" alt="Version control package">
- Be sure to clear the package manager search bar if packages are not being listed.  
  <img :src="$withBase('/images/unity-auto-extract/unity-package-manager-filter.png')" alt="Package manager filter">

The quickytools Character setup plugin depends on the new Unity input system so you need to accept and install it if your project doesn't use it. This <b>WILL</b> make changes to your project.
<img :src="$withBase('/images/unity-auto-extract/unity-accept-new-input-system-changes.png')" alt="Accept Unity input system changes">

### Setup auto extract folder
1. Create an Asset folder `quickytools/Character/AutoExtract`  
   <img :src="$withBase('/images/unity-auto-extract/unity-auto-extract-dir.png')" alt="Create AutoExtract folder" />
1. Create character on [quickytools](https://www.quickytools.com/character) and download the character model or use this [default character model](/models/human-animate-1.0.5.fbx).
1. Drag the downloaded quickytools Character model file into the AutoExtract folder to perform auto extraction. Character models outside the AutoExtract folder can use the `Tools > quickytools > Character > Extract character` option.  
   <img :src="$withBase('/images/unity-auto-extract/unity-extracted-character.png')" alt="Drag characters into AutoExtract folder" />
1. A Unity prefab of the character is created with the same name as the downloaded character model. Files in `base-assets` supports the character prefab so modify these files only if you understand how it affects the generated prefab.  

### Generate demo scene
1. Select the downloaded Character model file in the Project tab in Unity and select `Tools > quickytools > Character > Generate character scene`.  
   <img :src="$withBase('/images/unity-auto-extract/unity-select-asset-generate-scene.png')" alt="Generate character scene" />
1. A new scene should be created.  
   <img :src="$withBase('/images/unity-auto-extract/unity-generated-character-scene.png')" alt="Generated scene" />
1. Open the generated scene (if not loaded automatically). Press play (Cmd+P or Ctrl+P).  
    <img :src="$withBase('/images/unity-auto-extract/unity-play-on.png')" alt="Play mode">  
   <video width="512" autoplay loop>
     <source :src="$withBase('/images/unity-auto-extract/unity-generated-character-scene.mp4')" type="video/mp4" />
     <source :src="$withBase('/images/unity-auto-extract/unity-generated-character-scene.webm')" type="video/webm" />
   </video>
- Move the character with `WASD` keys on the keyboard.
- Press spacebar to jump.
- The `-+` keys changes the speed of movement as well as the movement animation.
