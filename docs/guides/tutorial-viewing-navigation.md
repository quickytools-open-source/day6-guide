---
lang: en-US
title: 3D Character Viewing Navigation
description: Tutorial on camera control
---

# {{ $frontmatter.title }}

This video tutorial will walk through our 3D character creator tool camera control.

### View Navigation Controls
<ul>
  <li>Rotate with alt+drag</li>
  <li>Pan with middle mouse button+drag</li>
  <li>Zoom with middle mouse button scroll</li>
  <li>First person navigation with WASD+QE keys</li>
  <li>Reframe the character or body section with pressing F key</li>
</ul>

<div class="video-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Y_aa3wpszQs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
