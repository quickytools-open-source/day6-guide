const { defaultTheme } = require('@vuepress/theme-default')

module.exports = {
  theme: defaultTheme({
    lang: 'en-US',
    title: 'quickytools',
    description: 'Create characters in the browser',
    logo: '/images/quickytools-logo.svg',
    contributors: false,
    lastUpdated: false,
    navbar: [
      {
        text: 'Blog',
        link: '/#blog',
      },
      {
        text: 'Guides',
        link: '/#guides',
      },
      // NavbarGroup
      // {
      //   text: 'Group',
      //   children: ['/group/foo.md', '/group/bar.md'],
      // },
      // string - page file path
      // '/bar/README.md',
    ],
    sidebar: false,
    head: [
      [
        'script',
        {
          async: true,
          src: 'https://www.googletagmanager.com/gtag/js?id=G-HRSMLWCD16',
        },
      ],
      [
        'script',
        {},
        [
          "window.dataLayer = window.dataLayer || [];\nfunction gtag(){dataLayer.push(arguments);}\ngtag('js', new Date());\ngtag('config', 'G-HRSMLWCD16');",
        ],
      ],
    ],
  })
}
