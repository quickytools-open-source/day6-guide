# Issues

See and follow [known issues](https://gitlab.com/quickytools-open-source/day6-guide/-/issues).

If you come across a new issue send an email to our [service desk](mailto:incoming+quickytools-open-source-day6-guide-27384926-issue-@incoming.gitlab.com) with steps to reproduce it and we'll add it to our known issues.
