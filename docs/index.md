---
lang: en-US
title: quickytools
description: Create characters in the browser
---
## 3D characters easy as pie.

<div class="post-feed">

<div class="post hero-post">
    <a href="./blogs/pose-feature.html">
        <img class="post-img" :src="$withBase('/images/Blogs/character-pose.png')" alt="Character posed" />
        <p class="title" >Characters are ready for posing</p>
    </a>
    <p class="text">Add a bit of life to your creations. Characters are more than airplane arms.</p>
</div>

<div class="post previous-post">
    <!-- https://stocksnap.io/photo/bird-perched-DT92LCGQZX -->
    <a href="./blogs/published-prototype.html">
        <img class="post-img" :src="$withBase('/images/Blogs/bird-on-post.png')" alt="Out the gate" />
        <p class="title">Prototype is released</p>
    </a>
    <p class="text">Here we go! Characters in your browser! Sweat inducing GPUs not required.</p>
</div>

</div>

## Guides

<div class="post-feed">

<div class="post previous-post">
    <a href="./guides/configure-unity-import.html">
        <img class="post-img" :src="$withBase('/images/Blogs/Import-guide-4.png')" alt="Character Import to Unity" />
        <p class="title" >Instructions on setting up Unity Basic Guide</p>
    </a>
    <p class="text">Load qharacters in Unity</p>
</div>

</div>

<div class="footer-bar">
    <a href="./issues.html">Report issue</a>
    <a href="https://twitter.com/quickyCharacter"> <img class="social-logo" :src="$withBase('/images/social-icons/twitter-logo.png')" title="Follow on Twitter" alt="Twitter"/></a>
    <a href="https://www.youtube.com/channel/UC9WkTKxvpSuPOqcwLnMkuog"> <img class="social-logo" :src="$withBase('/images/social-icons/youtube-logo.png')" title="Subscribe on YouTube" alt="YouTube"/></a>
    <a href="https://discord.gg/jpsvVk2hRu"> <img class="social-logo" :src="$withBase('/images/social-icons/discord-logo.png')" title="Join our Discord Channel" alt="Discord"/></a>
</div>
