---
lang: en-US
title: Character prototype
description: Announce completion of the prototype and where/how to access
---

The prototype is **complete!** Go and create a character from your [browser](https://www.quickytools.com/character) in Chrome or Edge. Other browsers coming soon.

Press the info button for instructions including how to setup [1-click importing into Unity](../guides/configure-unity-import.html).
