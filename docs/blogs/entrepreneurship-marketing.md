---
lang: en-US
title: Entrepreneurship and Marketing
description: Searching for success
date: 2021-11-6
---

# Hey y’all! 2nd blog posting here and hope you enjoy it.

## Life of an entrepreneur - Facing burnout and moving on

In January of 2021, I was in the final semester of my MBA. At the beginning of my entrepreneurship class, a former classmate and longtime friend, Qui, had reached out about an opportunity to use his service for my class project. The service proposed was a browser-based 3D character creator that simplifies the life of game developers and 3D character animators.

At first, working through the business case for this project, we had to validate if this was even a real issue. After several discussions, I realized that Qui has been at this project for quite some time. Qui discovered how much of a pain it was to develop his own video game and animated stories due to the lack of affordable and usable tools for building 3D characters.

Speed up to the present day. We are working towards a Minimum Viable Product (MVP). Recent talks with Qui showed that a push for progress does take a toll on your mind and well-being. The risk of burnout is always present and the feeling of progress slowing down is creeping in. This is a difficult situation to deal with as you are continually faced with the decision of quitting or continuing. 

Many questions arise. What is enough progress or not? How to measure progress? Can I reach my next milestone? Oh wait, that other company took 5 years to get to that point. Hmm…can I find someone else to help with building the MVP? As the Founder, all these thoughts continue to push to the forefront of Qui's mind to the point of self-doubt. At this point, experience as an entrepreneur was Qui's holy grail. He has learned to make a proper assessment of his emotional state and know when a break is needed. Keeping his eye on the prize and continuing to remember that entrepreneurship is not a sprint but a multi-year marathon. The grind continues as his experiences tell him that small wins and knowing that the product is needed will provide the energy to continue moving forward.

## Building social media presence - Promoting quickytools

These past few months have been a fun experience with learning about different social media platforms. Let's walk through the different ones we’re on and what we've discovered so far.

**Twitter** - We initially started with a Twitter account. The hunt for fellow big names in this environment was on and to start following them. We went off of YouTube videos and found their other social media accounts to follow first and see what kind of posts they share on their pages.

<img :src="$withBase('/images/Blogs/Twitter.jpg')" alt="Twitter" />

**Facebook** - Learning Facebook was an interesting experience as we had initially a separate account created for the company page. The user interface was overwhelming and cluttered to the point that there was too much happening from the administrator's view. Countless hours were spent searching for groups that are involved in 3D characters, Unity, and game development. This was an important first step to learn what hashtags people were using when posting in these groups. At a later time, the page was closed after realizing that not much traffic was coming through from a few of our posts. We decided to create a new page that was linked to my account so that I can create some traffic off the bat by having my inner circle added to the page. 

<img :src="$withBase('/images/Blogs/facebook.jpg')" alt="Facebook" />

**LinkedIn** - LinkedIn was found to be more of a professional site and harder to get traffic through as the hashtags didn’t drive enough engagement. The vibe we got from searching groups in LinkedIn is that the groups were more job opportunity-oriented and focused on professional networking environments. The good news is that we did create our company page and added ourselves to the company as employees to make it official!

<img :src="$withBase('/images/Blogs/LinkedIn.jpg')" alt="LinkedIn" />

**Instagram** - Instagram had a different feel than the other platforms, where it seems that hashtags have a big influence on getting viewers. Across all of the platforms, so far, Instagram has yielded the most rapid increase in traffic.

<img :src="$withBase('/images/Blogs/Instagram.jpg')" alt="Instagram" />
