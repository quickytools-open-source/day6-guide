---
lang: en-US
title: Pose feature
description: Announce completion of pose feature
---

A character without pose is a poem without structure, meandering for meaning.

Over a month of toiling away on inverse kinematics and we finally **got it done!** With quickytools Character Pose you can now express the [thrill of a chase](https://www.quickytools.com/character/open/alum9DtKlwh5ZU06eMRW)  
<img :src="$withBase('/images/pose-marketing/thrilling-chase.png')" alt="Thrilling chase" />

the [elegance of a twirl](https://www.quickytools.com/character/open/iiALQvYwmEuyK2N715X4)  
<img :src="$withBase('/images/pose-marketing/elegant-twirl.png')" alt="Elegant twirl" />

or the [precariousness of a catch](https://www.quickytools.com/character/open/8W4pjNF8DDjz2C1zGXpg)  
<img :src="$withBase('/images/pose-marketing/precarious-catch.png')" alt="Precarious catch" />

All on your desktop browser (if it supports WebGL 2).

Use simple controls to drag limbs into position or change facing directions. Joint constraints are minimal for maximum expression. Paired with our character creator you can shape and pose the forms of your wildest dreams and nightmares. 

If you are interested in creating characters easily in the browser sign up for our [mailing list](https://www.quickytools.com/character-mailing-list) and we’ll send updates as we make progress. Reach out to us on [Discord](https://discord.gg/SGkHAKaeZh) if you have feedback regarding ease of use or what you wish for next that will speed up character creation.
