---
lang: en-US
title: Introductions - Nice to meet you
description: this is the first blog
date: 2021-09-28
---

<h1>HELLO!</h1>

This is our first post for quickytools Character!

<u><h2><b>What we do</b></h2></u>

Have you tried creating a 3D character? Not sure where to start or downloaded different desktop applications and then still not sure what to do? That’s where we come in. Our goal is to simplify the process of generating 3D characters so that the characters are plug and play ready for any game content creator so that you can focus on more important things. The characters will be fully adjustable, animation ready, and controls set up to import into your game. At the moment, we are still starting out and currently have the capability for a low poly character to have their body features adjusted and recently able to rig a character for animation. There will be plenty more features and capabilities in the future and expansion beyond games so stay tuned!

<b><h2><u>Who we are</u></h2></b>

quickytools Characters was founded by Qui Van. He is a software engineer with over a decade of experience in native mobile app and full stack web development. Graduated from Florida State University with a BS in Mechanical Engineering degree followed by a career in government and private industry developing his software skills. He has recent experiences as a Lead software engineer for a few companies. Currently, Qui is a freelancer and working on bringing quickytools Characters to market. For myself, I met Qui at FSU and we graduated together! I’m still working in the aerospace industry, but in the process of finishing my MBA, I took a class in entrepreneurship which opened a new door of opportunity. Qui and I tagged up as I used quickytools Character as my semester project and since then we continued to stay the course and push!

<b><h2><u>Recent events</u></h2></b>

This past week, we created a new Facebook page for our business. The prior page was created through another Facebook account, but later realized that I could use my personal account instead. Doing so, I could tap into my own network first to help grow the page with more traffic. The second time around setting up a page was a bit simpler with adding the company information, pictures, first post, etc.

[Facebook Page](https://www.facebook.com/QuickytoolsCharacter)

Also in the process of growing our start up, we ran into a grant provided by Unreal Engine Epic MegaGrant. This was a really neat opportunity to get some funding support! The application process was pretty straightforward and turnaround time for a response is reasonable. The big takeaway of this application was that Unreal Engine wants to see their platform grow and grow the 3-D space. Grants can go up to $25k, but if more funding is needed, a detailed plan is required to break out what the funding will go towards. Stay tuned!

[Unreal Engine Epic MegaGrant](https://www.unrealengine.com/en-US/megagrants)
